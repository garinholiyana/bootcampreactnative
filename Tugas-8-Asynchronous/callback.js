function readBooks(time, book, callback ) {
    console.log(`saya membaca ${book.name}`)
    setTimeout(function() {
        let sisaWaktu = 0
        if (time > book.timeSpennt) {
            sisaWaktu = time - book.timeSpennt
            console.log(`saya sudah membaca ${book.name}, sisa waktu saya ${sisaWaktu}`)
            callback(sisaWaktu)
        } else {
            console.log(`waktu saya habis`)
        }
    }, book.timeSpent)
}

module.exports = readBooks