//If-else

var nama = "John"
var peran = "Guard"

if(nama == "") {
    console.log("Nama harus diisi!") 
} else if (peran == '') {
    console.log("Halo " + nama + " Pilih peranmu untuk memulai game!")
} else if (peran == 'Penyihir') {
    console.log("Selamat datang di Dunia Werewolf, " + nama)
    console.log("halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!")
} else if (peran == 'Guard') {
    console.log("Selamat datang di Dunia Werewolf, " + nama)
    console.log("halo Guard " + nama + ", kamu akan melindungi temanmu dari serangan werewolf")
} else if (peran == 'Werewolf') {
    console.log("Selamat datang di Dunia Werewolf, " + nama)
    console.log("Halo Werewolf " + nama + ", kamu akan memakan mangsa setiap malam")
}


//Switch Case

var hari = 21;      // assign nilai variabel tanggal di sini! (dengan angka antara 1 - 31) 
var bulan = 1;      // assign nilai variabel bulan di sini! (dengan angka antara 1 - 12)
var tahun = 2000;   // assign nilai variabel tahun di sini! (dengan angka antara 1900-2200)

switch(bulan) {
    case 1:bulan = 'Januari'; break;
    case 2:bulan = 'Februari'; break;
    case 3:bulan = 'Maret'; break;
    case 4:bulan = 'April'; break;
    case 5:bulan = 'Mei'; break;
    case 6:bulan = 'Juni'; break;
    case 7:bulan = 'Juli'; break;
    case 8:bulan = 'Agustus'; break;
    case 9:bulan = 'September'; break;
    case 10:bulan = 'Oktober'; break;
    case 11:bulan = 'November'; break;
    case 12:bulan = 'Desember'; break;
    default:bulan = 'bulan tidak ditemukan'
}
console.log(hari + " " + bulan + " " + tahun)