console.log('\n ----Tugas 4 - Functions----')

// No. 1
console.log ('\n ----------No. 1----------\n')

function teriak() {
    return "Halo Sanbers!"
}
console.log (teriak())

// No. 2
console.log ('\n ----------No. 2----------\n')

function kalikan(num1, num2) {
    hasilKali = num1 * num2
    return hasilKali
}
var num1 = 12
var num2 = 4
var hasilKali = kalikan(num1, num2)
console.log(hasilKali)

// No. 3
console.log ('\n ----------No. 3----------\n')

function introduce(name, age, address, hobby) {
    perkenalan = 'Nama saya ' + name + ', umur saya ' + age + ' tahun, alamat saya di ' + address + ', dan saya punya hobby yaitu ' + hobby + '!'
    return perkenalan
}
var name = 'agus'
var age = 30
var address = 'Jln. Malioboro, Yogyakarta'
var hobby = 'Gaming'
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) 

console.log('\n')
