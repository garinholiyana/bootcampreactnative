//Soal 1
console.log('\n----------Soal 1----------\n')

function range (startNum, finishNum) {
    var rA = [];
    if (startNum > finishNum){
        var rL = startNum - finishNum +1;
        for (var i = 0; i < rL; i++){
            rA.push(startNum - i)
        }
    } else if (startNum < finishNum) {
        var rL  = finishNum - startNum + 1;
        for (var i = 0; i < rL; i= i+1) {
            rA.push(startNum + i)
        }
    }else if (!startNum != finishNum){
        return -1
    }
    return rA 
}


console.log(range(1, 10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range())

//Soal 2
console.log('\n----------Soal 2----------\n')

function rangeWithStep (startNum, finishNum, step) {
    var rA = [];
    if (startNum > finishNum) {
        var cN = startNum;
        for (var i = 0; cN >= finishNum; i++) {
            rA.push (cN)
            cN -= step
        }
    }else if (startNum < finishNum){
        var cN = startNum;
        for(var i = 0; cN <= finishNum; i++){
            rA.push(cN)
            cN += step
        }
        
    }else if( !startNum != finishNum != step){
        return -1
    }
    return rA
}

console.log(rangeWithStep(1, 10, 2)) 
console.log(rangeWithStep(11, 23, 3)) 
console.log(rangeWithStep(5, 2, 1)) 
console.log(rangeWithStep(29, 2, 4))

//Soal 3
console.log('\n----------Soal 3----------\n')

function sum(startNum, finishNum, step){
    var rA =[ ]
    var dostance;
    if(!step){
        distance=1
    }else {
        distance = step
    }
    if (startNum > finishNum) {
        var cN = startNum
        for (var i = 0; cN >=finishNum; i++) {
            rA.push(cN)
            cN -= distance
        }
    }else if (startNum < finishNum) {
        var cN = startNum
        for (var i = 0; cN <=finishNum; i++) {
            rA.push(cN)
            cN += distance
        }
    }else if (!startNum && !finishNum && !step) {
        return 0
    } else if  (startNum) {
        return startNum
    }
    var total = 0;
    for (var i = 0; i < rA.length; i++) {
        total = total + rA[i]
    }
    return total
}


console.log(sum(1,10)) 
console.log(sum(5, 50, 2)) 
console.log(sum(15,10)) 
console.log(sum(20, 10, 2)) 
console.log(sum(1))
console.log(sum()) 


//Soal 3
console.log('\n----------Soal 4----------\n')

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling (data) {
    var dataLength = data.length
    for (var i= 0; i< dataLength; i++){
        var id = 'Nomor Id :' + data[i][0]
        var nama = 'Nama Lengkap : ' + data[i][1]
        var ttl = 'TTL: ' + data [i][2] + '' + data [i][3]
        var hobi = 'Hobi : ' + data [i][4]
        console.log(id)
        console.log(nama)
        console.log(ttl)
        console.log(hobi)
    }
}


dataHandling(input)


//Soal 5
console.log('\n----------Soal 5----------\n')

function balikKata (kata) {
    var kataBaru =' '
    for (var i = kata.length -1; i >= 0; i--){
        kataBaru += kata[i]
    }
    return kataBaru
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

//Soal 6
console.log('\n----------Soal 6----------\n')

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
dataHandling2(input)

function dataHandling2(data){
    var newData = data
    var newName = data[1] + 'Elsharawy'
    var newProvince = 'Provinsi' + data[2]
    var gender ='Pria'
    var institusi = 'SMA International Metro'

    newData.splice(1, 1, newData)
    newData.splice(2, 1, newProvince)
    newData.splice(4, 1, gender, institusi)

    var aD = data [3]
    var newDate = aD.split('/')
    var monthNum = newDate[1]
    var monthname = ' '

    switch(monthNum){
        case '01':
            monthname = 'Januari'
            break;
        case '02':
            monthname = 'Februari'
            break;
        case '03':
            monthname = 'Maret'
            break;
        case '04':
            monthname = 'April'
            break;
        case '05':
            monthname = 'Mei'
            break;
        case '06':
            monthname = 'Juni'
            break;
        case '07':
            monthname = 'Juli'
            break;
        case '08':
            monthname = 'Agustus'
            break;
        case '09':
            monthname = 'September'
            break;
        case '10':
            monthname = 'Oktober'
            break;
        case '11':
            monthname = 'November'
            break;
        case '12':
            monthname = 'Desember'
            break;
            
    }
    var dateJoin = newDate.join('-')
    var dateArr = newDate.sort(function(value1, value2){
        value2 - value1
    })
    var editName = newName.slice (0, 15)
    console.log(newData)

    console.log(monthname)
    console.log(dateArr)
    console.log(dateJoin)
    console.log(editName)
}
