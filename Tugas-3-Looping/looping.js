//No. 1 Looping While
console.log('\n-----------No. 1 Looping While-----------')

console.log ('LOOPING PERTAMA')
var a = 2;
while(a <= 20) {
    console.log (a + ' - I love coding');
    a = a + 2
}

console.log ('LOOPING KEDUA')
var b = 20;
while(b >= 2) {
    console.log (b + ' - I will become a mobile developer')
    b = b - 2
}


//No. 2 Lopping menggunakan for
console.log('\n-----------No. 2 Lopping menggunakan for-----------')

for( var x = 1; x <=20; x++){
    if(x%2 == 0)
    console.log (x + ' - Berkualitas')
    else if (x%3 ==0)
    console.log (x + ' - I Love Coding')
    else if (x%2 != 0)
    console.log (x + ' - Santai')
}

//No. 3 Membuat Persegi Panjang #
console.log('\n-----------No. 3 Membuat Persegi Panjang #-----------')

var hasil = ''
for( var i = 1; i <=4; i++) {
    for(var j = 1; j <=9; j++){
    hasil +='#'
    }
hasil +='\n'
}
console.log(hasil)

console.log('\n-----------No. 4 Membuat Tangga-----------')

var hasil = ''
for( var i = 1; i <8; i++) {
    for(var j = 1; j <=i; j++){
    hasil +='#'
    }
hasil +='\n'
}
console.log(hasil)


//No. 5 Membuat Papan Catur
console.log('\n-----------No. 5 Membuat Papan Catur-----------')

for(var g = 1; g <=8; g++){
    if(g%2 == 0)
    console.log('# # # #')
    else
    console.log(' # # # #')
}





